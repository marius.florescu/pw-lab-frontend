# PW Lab Frontend

### How to run

- you need to have (Node.js)[https://nodejs.org/en] installed.
- clone the repository: `git clone https://gitlab.upt.ro/marius.florescu/pw-lab-frontend`
- install the dependencies: `npm install`
- start the local app: `npm run dev`

If everything went well, you should see in the terminal the following line:

```
  ➜  Local:   http://localhost:5173/
```

Control+Click (CMD+Click for macOS users) on the link in order to open it in the browser.

## Lab 1

The scope of the Lab 1 (frontend) is to create a simple React application with a component that accepts `name` as a prop.
At the end of the laboratory, everyone should be able to start the React app and see the result.

If you have never worked with React before and you want to learn more about it, **I highly recommend the following links**:

- https://react.dev/learn -> the official documentation of React, a great place to learn the basic concepts. It contains many interactive examples and illustration and it will make it significantly easier for you to wrap your head why and how things work. Additionally, it will be very helpful throughout the laboratory.
- https://react.dev/learn/tutorial-tic-tac-toe -> toc tac toe tutorial
- https://react.dev/learn/thinking-in-react -> if you've read the articles above, this will provide you some insights on how to think in React, how to break your UI into a component hierarchy and also see the process of going from a static version to a fully functional one.

## Lab 2

The scope of Lab 2 (frontend) is to add routing and authentication to our application.

The routing is done via the library `react-router-dom` and the authentication process is done by our backend which attaches a cookie containing the token to our browser.
In order to persist the authentication state, everytime the user comes into our application an API call is being made to the `/me` route, which check the cookie if it's valid and if it's not expired (can be seen in `src/providers/AuthProvider`).

The reason behind the `AuthProvider.jsx` is to facilitate access to our authentication state in our entire application. This can be seen as a global state, which does not need to be passed via props (as we saw in the example from our last laboraty, where then `name` property was being passed as a prop).
