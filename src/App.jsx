import { CssBaseline, CssVarsProvider } from "@mui/joy";
import axios from "axios";
import Router from "./Router";
import theme from "./theme";
import { Toaster } from "sonner";
import AuthProvider from "./providers/AuthProvider";

axios.defaults.baseURL = "http://localhost:8080/api";
axios.defaults.withCredentials = true;

function App() {
  return (
    <CssVarsProvider theme={theme} disableTransitionOnChange>
      <CssBaseline />
      <Toaster richColors />
      <AuthProvider>
        <Router />
      </AuthProvider>
    </CssVarsProvider>
  );
}

export default App;
