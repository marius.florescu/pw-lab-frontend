import { useEffect, useState } from "react";
import {
  Button,
  Card,
  CardActions,
  CardContent,
  FormControl,
  FormLabel,
  Input,
  Stack,
  Typography,
} from "@mui/joy";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { toast } from "sonner";
import { useUser } from "../providers/AuthProvider";

export default function Login() {
  const navigate = useNavigate();
  const { user, setUser } = useUser();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleLogin = async () => {
    try {
      const { data } = await axios.post("/login", {
        email,
        password,
      });
      setUser(data);
      navigate("/");
    } catch (err) {
      if (err.response?.data?.error) {
        toast.error(err.response.data.error);
      }
    }
  };

  useEffect(() => {
    if (user) {
      navigate("/");
    }
  }, [user]);

  return (
    <Stack height={1} justifyContent="center" alignItems="center" px={4}>
      <Card
        sx={(theme) => ({
          width: "100%",
          maxWidth: theme.breakpoints.values.sm,
        })}
      >
        <CardContent>
          <Typography level="h1" textAlign="center">
            Login
          </Typography>
          <Stack spacing={2} mt={4}>
            <FormControl>
              <FormLabel>Email</FormLabel>
              <Input
                name="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                placeholder="myemail@mail.me"
              />
            </FormControl>
            <FormControl>
              <FormLabel>Password</FormLabel>
              <Input
                type="password"
                name="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                placeholder="Password"
              />
            </FormControl>
          </Stack>
          <CardActions>
            <Button onClick={handleLogin}>Login</Button>
          </CardActions>
        </CardContent>
      </Card>
    </Stack>
  );
}
