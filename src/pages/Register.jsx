import {
  Button,
  Card,
  CardActions,
  CardContent,
  FormControl,
  FormLabel,
  Input,
  Stack,
  Typography,
} from "@mui/joy";
import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { toast } from "sonner";
import { useUser } from "../providers/AuthProvider";

export default function Register() {
  const navigate = useNavigate();
  const { user } = useUser();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");

  const handleRegister = async () => {
    try {
      await axios.post("/register", {
        email,
        password,
        confirmPassword,
        firstName,
        lastName,
      });
      navigate("/login");
    } catch (err) {
      if (err.response?.data?.error) {
        toast.error(err.response.data.error);
      }
    }
  };

  useEffect(() => {
    if (user) {
      navigate("/");
    }
  }, [user]);

  return (
    <Stack height={1} justifyContent="center" alignItems="center" px={4}>
      <Card
        sx={(theme) => ({
          width: "100%",
          maxWidth: theme.breakpoints.values.sm,
        })}
      >
        <CardContent>
          <Typography level="h1" textAlign="center">
            Register
          </Typography>
          <Stack spacing={2} mt={4}>
            <FormControl>
              <FormLabel>Email</FormLabel>
              <Input
                name="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                placeholder="myemail@mail.me"
              />
            </FormControl>
            <FormControl>
              <FormLabel>Password</FormLabel>
              <Input
                type="password"
                name="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                placeholder="Password"
              />
            </FormControl>
            <FormControl>
              <FormLabel>Confirm Password</FormLabel>
              <Input
                type="password"
                name="confirmPassword"
                value={confirmPassword}
                onChange={(e) => setConfirmPassword(e.target.value)}
                placeholder="Confirm Password"
              />
            </FormControl>
            <FormControl>
              <FormLabel>Firstname</FormLabel>
              <Input
                name="firstname"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                placeholder="John"
              />
            </FormControl>
            <FormControl>
              <FormLabel>Lastname</FormLabel>
              <Input
                name="lastname"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
                placeholder="myemail@mail.me"
              />
            </FormControl>
          </Stack>
          <CardActions>
            <Button onClick={handleRegister}>Register</Button>
          </CardActions>
        </CardContent>
      </Card>
    </Stack>
  );
}
